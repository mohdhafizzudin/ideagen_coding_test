package src;

import java.util.Arrays;
import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        //System.out.println("1 + 1 = " + calculate("1 + 1"));
        //System.out.println("2 * 2 = " + calculate("2 * 2"));
        System.out.println("1 + 2 + 3 = " + calculate("1 + 2 + 3"));
        //System.out.println("6 / 2 = " + calculate("6 / 2"));
        //System.out.println("result (11.5 + 15.4) + 10.1 = " + calculate("(11.5 + 15.4) + 10.1"));
        //System.out.println("result 10 - ( 2 + 3 * ( 7 - 5 ) ) = " + calculate("10 - ( 2 + 3 * ( 7 - 5 ) )"));
    }

    public static double calculate(String expression) {
        String[] tokens = expression.replaceAll("\\s+", "").split("(?<=[-+*/()])|(?=[-+*/()])");
        System.out.println("tokens: " + Arrays.toString(tokens));
        Stack<Double> values = new Stack<>();
        Stack<Character> operators = new Stack<>();
        handleToken(tokens, values, operators);
        postHandleOperator(operators, values);
        return values.pop();
    }

    private static boolean hasPrecedence(char op1, char op2) {
        return (op1 == '*' || op1 == '/') && (op2 == '+' || op2 == '-');
    }

    private static double applyOperator(char operator, double value2, double value1) {
        return switch (operator) {
            case '+' -> value1 + value2;
            case '-' -> value1 - value2;
            case '*' -> value1 * value2;
            case '/' -> {
                if (value2 == 0) {
                    throw new ArithmeticException("Division by zero");
                }
                yield value1 / value2;
            }
            default -> throw new IllegalArgumentException("Invalid operator: " + operator);
        };
    }

    private static void handleToken(String[] tokens, Stack<Double> values, Stack<Character> operators) {
        for (String token : tokens) {
            if (token.isEmpty()) {
                continue;
            }
            char c = token.charAt(0);
            if (Character.isDigit(c) || (c == '-' && token.length() > 1 && Character.isDigit(token.charAt(1)))) {
                values.push(Double.parseDouble(token));
            } else if (c == '(') {
                operators.push(c);
            } else if (c == ')') {
                while (!operators.isEmpty() && operators.peek() != '(') {
                    values.push(applyOperator(operators.pop(), values.pop(), values.pop()));
                }
                operators.pop();
            } else {
                while (!operators.isEmpty() && hasPrecedence(c, operators.peek())) {
                    values.push(applyOperator(operators.pop(), values.pop(), values.pop()));
                }
                operators.push(c);
            }
            System.out.println("Values Stack: " + values.toString());
            System.out.println("Operators Stack: " + operators.toString());
            System.out.println("***************");
        }
    }

    private static void postHandleOperator(Stack<Character> operators, Stack<Double> values) {
        while (!operators.isEmpty()) {
            values.push(applyOperator(operators.pop(), values.pop(), values.pop()));
        }
    }
}